package com.horsego.doadoa;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Objects;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class InstDescAcitivity extends AppCompatActivity{
    private String nome_inst;
    private String id_inst;
    private WebView webViewDescricao;
    private ImageView imageView_header_logo;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if(extras==null)
        {
            nome_inst = null;
        }
        else
        {
            id_inst = extras.getString(getString(R.string.json_inst_id));
            nome_inst = extras.getString(getString(R.string.json_inst_nome));
        }

        setContentView(R.layout.instituicao_descricao);

        Objects.requireNonNull(getSupportActionBar()).setTitle(nome_inst);
        webViewDescricao = findViewById(R.id.texto_descritivo);
        new GetDataTask().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class GetDataTask extends AsyncTask<Void, Void, String> {
        private OkHttpClient client;
        private String url;
        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            client =  new OkHttpClient();
            dialog = new ProgressDialog(InstDescAcitivity.this);
            // Mensagem fofa
            dialog.setMessage(getString(R.string.info_loading_descricao));
            // Mostra tela de carregando
            dialog.show();
            url = getString(R.string.server_name)+getString(R.string.server_port)+getString(R.string.api_instituicao_get)+"?cnpj"+id_inst;
            Log.v("url:",url);
        }
        @Override
        protected String doInBackground(Void... params) {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            if(s==null) {
                Toast.makeText(getApplicationContext(), R.string.error_internet_fail,Toast.LENGTH_LONG).show();
                return;
            }
            try {
                JSONObject arr = new JSONObject(s);
                webViewDescricao.loadDataWithBaseURL("",arr.getString(getString(R.string.json_inst_descricao)), getString(R.string.utils_mime_type), getString(R.string.utils_encoding), "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialog.dismiss();
        }
    }

    public void doarpaginadedoar(View view)
    {
        Intent i = new Intent(InstDescAcitivity.this,ConfirmacaoActivity.class);
        i.putExtra(getString(R.string.json_inst_nome), nome_inst);
        i.putExtra(getString(R.string.json_inst_id), id_inst);
        startActivity(i);
    }
}
