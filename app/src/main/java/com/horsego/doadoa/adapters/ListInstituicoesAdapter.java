package com.horsego.doadoa.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.horsego.doadoa.MainActivity;
import com.horsego.doadoa.R;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;

// Criado o adapter da lista com fotos, usados para exibir a foto+texto da instituição
// Em formato de lista

public class ListInstituicoesAdapter extends ArrayAdapter<String> {
    // Contexto
    private final Activity context;
    // Id instituição
    private final int[] id_instituicao;
    // Descrição da instituição
    private final String[] desc;
    // Nome da instituição
    private final String[] nome_instituicao;
    // Imagem da instituição
    private final String[] image_instituicao;

    // Construtor que popula todos os parâmetros acima
    public ListInstituicoesAdapter(Activity context, String[] web, String[] imageProd, int[] id, String[] desc) {
        super(context, R.layout.list_item, web);
        this.context = context;
        this.nome_instituicao = web;
        this.image_instituicao = imageProd;
        this.id_instituicao = id;
        this.desc = desc;
    }

    @NonNull
    @Override
    // Devolve a linha já formatada
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        // Seleciona o visual XML a ser usado, neste caso é: R.layout.list_item
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView= inflater.inflate(R.layout.list_item, null, true);
        // Faz o link entre o visual e o código Java
        TextView txtTitle =  rowView.findViewById(R.id.list_item_txt);
        CircleImageView imageView =  rowView.findViewById(R.id.list_item_img);
        // Coloca o valor recebido na linha atual
        txtTitle.setText(nome_instituicao[position]);
        // Coloca a foto recebida no ícone redondo de foto
        Picasso.get().load(image_instituicao[position]).into(imageView);
        // Retorna a linha formatada
        return rowView;
    }
    // Retorna os itens em que você "clica"
    public String getItemAtPosition(int pos)
    {
        return nome_instituicao[pos];
    }
    public String getDescAtPosition(int pos)
    {
        return desc[pos];
    }
    public String getImageAtPosition(int pos)
    {
        return image_instituicao[pos];
    }
    public int getIdAtPosition(int pos)
    {
        return id_instituicao[pos];
    }


}
