package com.horsego.doadoa;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.horsego.doadoa.adapters.ListInstituicoesAdapter;

import org.json.JSONArray;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private ListView list_inst;
    private SwipeRefreshLayout swipeLayout;
    GetDataTask a;
    int[] inst_id_list;
    String[] inst_nome_list;
    String[] inst_desc;
    String[] inst_photo_url;
    Bitmap[] bmap;
    ListInstituicoesAdapter lia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Adiciona o visual como activity_main
        setContentView(R.layout.activity_main);
        // Pega a toolbar
        Toolbar toolbar =  findViewById(R.id.toolbar);
        // Adiciona a toolbar como padrão
        setSupportActionBar(toolbar);
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        list_inst = findViewById(R.id.list_instituicoes);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        a = new GetDataTask();
        swipeLayout = findViewById(R.id.swiperefresh);
        swipeLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary,R.color.colorGray);
        swipeLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //Apenas se a thread não está sendo executada
                        updateInstituicoes();
                    }
                }

        );

        list_inst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    Intent i = new Intent(MainActivity.this, InstDescAcitivity.class);
                    i.putExtra(getString(R.string.json_inst_descricao), lia.getDescAtPosition(position));
                    i.putExtra(getString(R.string.json_inst_nome), lia.getItemAtPosition(position));
                    i.putExtra(getString(R.string.json_inst_id), lia.getIdAtPosition(position));
                    i.putExtra(getString(R.string.json_inst_logo), lia.getImageAtPosition(position));
                    startActivity(i);
                }catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Opa opa, pera aí sabichão, parece que vc ocasionou um erro bem serelepe",Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }


            }
        });
        updateInstituicoes();
    }

    private void updateInstituicoes()
    {
        swipeLayout.setRefreshing(true);
        if (!(a.getStatus() == AsyncTask.Status.RUNNING)) {
            try {
                new GetDataTask().execute();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), R.string.error_internet_fail, Toast.LENGTH_SHORT).show();
                swipeLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        if(id == R.id.nav_instituitions)
        {

        }
        if(id == R.id.nav_my_donations)
        {

        }
        if(id == R.id.nav_feedback)
        {

        }
        if(id == R.id.nav_about)
        {

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    private class GetDataTask extends AsyncTask<Void, Void, String> {
        private OkHttpClient client;
        private String url;
        @Override
        protected void onPreExecute() {
            client =  new OkHttpClient();
            url = getString(R.string.server_name)+getString(R.string.server_port)+getString(R.string.api_getall);
        }
        @Override
        protected String doInBackground(Void... params) {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response ;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            if(s==null) {
                Toast.makeText(getApplicationContext(), R.string.error_internet_fail,Toast.LENGTH_LONG).show();
                return;
            }
            try {
                JSONArray arr = new JSONArray(s);
                inst_id_list = new int[arr.length()];
                inst_nome_list = new String[arr.length()];
                inst_desc = new String[arr.length()];
                inst_photo_url = new String[arr.length()];
                bmap = new Bitmap[arr.length()];


                for (int i = 0; i < arr.length(); i++)
                {

                    inst_nome_list[i] = arr.getJSONObject(i).getString(getString(R.string.json_inst_nome));
                    inst_desc[i] = arr.getJSONObject(i).getString(getString(R.string.json_inst_descricao));
                    inst_photo_url[i] = arr.getJSONObject(i).getString(getString(R.string.json_inst_logo));
                    inst_id_list[i] = arr.getJSONObject(i).getInt(getString(R.string.json_inst_id));
                }
                lia = new ListInstituicoesAdapter(MainActivity.this,inst_nome_list,inst_photo_url,inst_id_list,inst_desc);
                list_inst.setAdapter(lia);
            } catch (Exception e) {
                e.printStackTrace();
                Log.v("URL:",s);
            }
            swipeLayout.setRefreshing(false);

        }
    }
}
